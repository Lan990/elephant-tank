﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Enum for multiple controls sets
public enum ControlsSceam {KBMP1,ConP1};

public class PlayerControl : MonoBehaviour {
	private TankData data;

	//Enum variable holder
	public ControlsSceam controls;
	// Use this for initialization
	void Start () {
		data = GetComponent<TankData> ();
	}

	// Update is called once per frame
	void Update () {
		MoveInput ();
		Shoot ();
	}

	void MoveInput()
	{
		//New vector 3 variable set to (0,0,0)
		Vector3 moveVector = Vector3.zero;
		Vector3 tankTurn = Vector3.zero;

		switch (controls) {
		//First case for Keyboard and mouse.
		case ControlsSceam.KBMP1:

			if (Input.GetKey(KeyCode.W)) {
				moveVector = data.tf.forward * data.moveSpeed;
				SendMessage ("Move", moveVector, SendMessageOptions.DontRequireReceiver);//Calls the Move function in the mover
			}

			if (Input.GetKey(KeyCode.S)) {
				moveVector = data.tf.forward * -data.backwordsMoveSpeed;
				SendMessage ("Move", moveVector, SendMessageOptions.DontRequireReceiver);
			}

			if (Input.GetKey(KeyCode.D)) {
				tankTurn = data.tf.up * data.turnSpeed;
				SendMessage ("Rotate", tankTurn, SendMessageOptions.DontRequireReceiver);//Calls the rotate function in the mover
			}
			if (Input.GetKey(KeyCode.A)) {
				tankTurn = data.tf.up * -data.turnSpeed;
				SendMessage ("Rotate", tankTurn, SendMessageOptions.DontRequireReceiver);
			}

			break;
			//Secound case for controller
		case ControlsSceam.ConP1:
			//Setting the axis input to a veriable
			float xAxis = Input.GetAxis ("Horizontal");
			float yAxis = Input.GetAxis ("Vertical");

			if (yAxis > 0) {
				moveVector = data.tf.forward * data.moveSpeed;
				SendMessage ("Move", moveVector, SendMessageOptions.DontRequireReceiver);
			}

			if (yAxis < 0) {
				moveVector = data.tf.forward * -data.backwordsMoveSpeed;
				SendMessage ("Move", moveVector, SendMessageOptions.DontRequireReceiver);
			}

			if (xAxis > 0) {
				tankTurn = data.tf.up * data.turnSpeed;
				SendMessage ("Rotate", tankTurn, SendMessageOptions.DontRequireReceiver);
			}
			if (xAxis < 0) {
				tankTurn = data.tf.up * -data.turnSpeed;
				SendMessage ("Rotate", tankTurn, SendMessageOptions.DontRequireReceiver);
			}
			break;


			/*if (yAxis > 0) 
			{
				data.tf.Rotate (new Vector3 (0, data.turnSpeed * Time.deltaTime, 0));
			}*/
		}

	}




	//
	void Shoot()
	{//Checks for the shoot input and checks how long it's been since the last press.
		if (Input.GetAxis ("Fire1") > 0 && Time.time > data.nextFire)
		{
			//Debug.Log ("Shooting");
			SendMessage ("Fire", SendMessageOptions.DontRequireReceiver);//Calls the rotate function in the mover
			data.nextFire = Time.time + data.fireRate;
		}
	}
}
