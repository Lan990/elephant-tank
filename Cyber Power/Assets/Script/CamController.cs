﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamController : MonoBehaviour {

	public Vector3 offset;
	[Tooltip("Camera to center")]
	public Camera cam;
	private PlayerControl[] players;
	private Transform tf;
	public Vector3 center;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
	}

	// Update is called once per frame
	void Update () {
		players = GameObject.FindObjectsOfType (typeof(PlayerControl)) as PlayerControl[];

		center = Vector3.zero;
		for (int i = 0; i < players.Length; i++) {
			center += players [i].transform.position;
		}
		center /= players.Length;
		cam.transform.position = center + offset;
	}
}
