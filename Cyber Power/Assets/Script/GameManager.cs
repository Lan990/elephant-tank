﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public static GameManager gM;
	[Header("Score Tracker")]
	public int score;

	// Insures only one of these exsist at a time.
	void Awake () {
		if (gM != null) {
			Destroy (gameObject);
		} else {
			gM = this;
			DontDestroyOnLoad (gameObject);
		}
	}

	
}
