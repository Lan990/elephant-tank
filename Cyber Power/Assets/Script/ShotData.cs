﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotData : MonoBehaviour {
	[HideInInspector]
	public Transform tf;
	[Tooltip("Strength at which shots will fly")]
	public float projectileForce;
	[Tooltip("Damege caused by shot")]
	public int shotdamege;
	[Tooltip("Max amount of time a shot may stay in sceen. ")]
	public float despawnTime;
	// Use this for initialization

	void Awake()
	{
		tf = GetComponent<Transform> ();
	}
	void Start()
	{
		Destroy (gameObject,despawnTime);
	}

}
