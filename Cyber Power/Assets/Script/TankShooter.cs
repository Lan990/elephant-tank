﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankShooter : MonoBehaviour {
	private TankData data;
	[SerializeField,Tooltip("Holder for the prefab of what's being shot")]
	private GameObject projectile;
	[SerializeField,Tooltip("Holder for projectile spwan.")]
	private Transform projectileSpawn;


	// Use this for initialization
	void Start () {
		data = GetComponent<TankData> ();
	}
	
	//Function called by Shoot in player control.
	public void Fire()
	{
		//Creates the shot
			GameObject shot = Instantiate (projectile, projectileSpawn.position, data.tf.rotation) as GameObject;
		Debug.Log ("Shooting");
			

	}
}
