﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class ShotBehavior : MonoBehaviour {

	private Rigidbody rB;
	private ShotData sData;

	// Use this for initialization
	void Awake () {
		sData = GetComponent<ShotData> ();
		rB = GetComponent<Rigidbody> ();
	}
	void Start () {
		
		//Propels shot forward using force and it's rigidbody
		rB.AddForce (sData.projectileForce * sData.tf.forward *Time.deltaTime);
	}
	
}
