﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {
	[HideInInspector]
	public CharacterController cC;
	private TankData data;

	// Use this for initialization
	void Start () {

		cC = GetComponent<CharacterController> ();
		data = GetComponent<TankData> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	//Function to move character 
	public void Move(Vector3 moveTo)//Gets the move vector from player controler
	{
		cC.SimpleMove (moveTo);
	}
	//Function to rotate character 
	public void Rotate(Vector3 turnTo)//Gets the move vector from player controler
	{
		data.tf.Rotate (turnTo);
	}
}
