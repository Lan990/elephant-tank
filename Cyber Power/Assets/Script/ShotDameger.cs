﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotDameger : MonoBehaviour {
	private ShotData sData;

	// Use this for initialization
	void Start () {
		sData = GetComponent<ShotData> ();

	}
	
	// Update is called once per frame

	void OnTriggerEnter (Collider other )
	{
		//Calls the Take Damage function in the take damage script.
		other.gameObject.SendMessage ("TakeDamage",sData.shotdamege,SendMessageOptions.DontRequireReceiver);
		Destroy (gameObject);
	}
}
